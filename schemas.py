from pydantic import BaseModel
from typing import List


class BlogBase(BaseModel):
    title: str
    body: str

    class Config():
        orm_mode = True


class Blog(BlogBase):
    class Config():
        orm_mode = True


class ShowBlog(BaseModel):
    class Config():
        orm_mode = True
